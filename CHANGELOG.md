# isa-l 0.2.0

## Breaking Changes
- Change signature of `ec_init_tables` and `ec_encode_data` to write to a buffer instead of allocating.
- Rename old allocating methods to `*_owned`.

### Added
- Use workspace.

### Fixed
- Gitlab badges

# 0.1.2

### Fixed
- Keep source directory read only.
- Don't symlink license

# 0.1.1

### Added
- Building `libisal` from source and linking statically.

# 0.1.0

### Added
- Dynamic linking of `libisal`.
- Add bindings for _Erasure Code_ methods.
