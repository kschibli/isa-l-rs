# isa-l-rs

Rust bindings to libisal, the Intel(R) Intelligent Storage Acceleration Library

Currently only bindings for _Erasure codes_ are implemented. _CRC_, _Raid_, 
_Compression_ and _De-compression_ might follow later (PRs welcome).

## Usage
```toml
# Cargo.toml
[dependencies]
isa-l = "0.1"
```

## Building

Dynamic linking to `libisal >= 2.14.1` is attempted otherwise it is built from source. Building from source requires
`nasm` or `yasm` (See https://github.com/intel/isa-l#prerequisites).

## Updating `libisal-sys` bindings

To update the bindings we use `bindgen` and update the resulting bindings manually:
 
- Install `bindgen`:
  ```console
  $ cargo install bindgen
  ```
- Install the new version of `libisal` on the system.
- Enter the `tools` directory:
  ```console
  $ cd tools
  ```
- Review `wrapper.h` and `bindgen.sh`
- Generate the new bindings
  ```console
  $ ./bindgen.sh > ../libisal-sys/lib.bindgen.rs
  ```
- Merge with old bindings, check docs, update / add tests
  ```console
  $ vimdiff ../libisal-sys/lib.bindgen.rs ../libisal-sys/lib.rs
  ```

