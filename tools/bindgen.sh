#!/usr/bin/env sh
bindgen \
    wrapper.h \
    --whitelist-function 'ec_encode_data' \
    --whitelist-function 'ec_init_tables' \
    --whitelist-function 'gf_gen_cauchy1_matrix' \
    --whitelist-function 'gf_invert_matrix' \
    --whitelist-function 'gf_mul' \
    --whitelist-function 'gf_gen_rs_matrix' \
    --whitelist-function 'gf_inv'\
    --distrust-clang-mangling
